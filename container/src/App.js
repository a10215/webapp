import React from "react";
import {
  BrowserRouter,
  Routes,
  Route,

} from "react-router-dom";
import { createBrowserHistory } from "history";
import Grid from '@mui/material/Grid';
//import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
//import Paper from '@mui/material/Paper';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import MicroFrontend from "./MicroFrontends.component";

import "./App.css";

const defaultHistory = createBrowserHistory();

const {
  REACT_APP_ANIMALS_HOST: animalsHost,

} = process.env;


/* const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
})); */

function Header() {
  return (
    <div className="banner">
      <h1 className="banner-title">Home</h1>
      <h4>Random pics of animals</h4>
    </div>
  );
}

/* function Animals({ history }) {
  return <MicroFrontend history={history} host={animalsHost} name="Animals" />;
} */

function Home() {



  return (
    <div>
      <CssBaseline />
      <Container fixed>
        <Header />


        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={2}>
            <Grid item xs={8}>
              <MicroFrontend  host={animalsHost} name="Animals" />
            </Grid>
            {/*  <Grid item xs={4}>
            <Item>xs=4</Item>
          </Grid>
          <Grid item xs={4}>
            <Item>xs=4</Item>
          </Grid>
          <Grid item xs={8}>
            <Item>xs=8</Item>
          </Grid> */}
          </Grid>
        </Box>
      </Container>
    </div>
  )
}

function App({ history = defaultHistory }) {
  return (
    <BrowserRouter>
      <React.Fragment>
        <Routes>
          <Route exact path="/" element={<Home history={history} />} />
        </Routes>
      </React.Fragment>
    </BrowserRouter>
  )
}

export default App;