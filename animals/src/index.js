import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import reportWebVitals from './reportWebVitals';
import RandomCat from './RandomAnimals.component';
//import * as serviceWorker from './serviceWorker';
import RandomAnimals from './RandomAnimals.component';

window.renderAnimals = (containerId, history) => {
  ReactDOM.render(
    <RandomAnimals history={history} />,
    document.getElementById(containerId),
  );
  //serviceWorker.unregister();
};

window.unmountAnimalss = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
};

if (!document.getElementById('Animals-container')) {
  ReactDOM.render(<RandomAnimals />, document.getElementById('root'));
  //serviceWorker.unregister();
}


