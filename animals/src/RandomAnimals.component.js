import React, { useState, useEffect } from "react";

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';


import "./App.css";

function RandomAnimals() {

  const [randomDogImg, setRandomDogImg] = useState(null);

  const fetchRandomDog = () => {
    setRandomDogImg("");
    fetch(`https://dog.ceo/api/breeds/image/random`)
      .then((res) => res.json())
      .then((dogInfo) => {
        console.log("info",dogInfo)
        setRandomDogImg(dogInfo.message);
      });
  };

  useEffect(() => {
    if (randomDogImg === null) {
      fetchRandomDog();
    }
  })

  return (

   <Card sx={{ maxWidth: 345 }}>
     {randomDogImg !== "" 
     ? (
          <CardMedia
          component="img"
          height="250"
          src={randomDogImg}
          alt="attention au chien"
        />
         
        ) 
        : (
          <div>Loading Image</div>
        )}
      
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
         Waf!
        </Typography>
        <Typography variant="body2" color="text.secondary">
         Bonjour:)
        </Typography>
      </CardContent>
      <CardActions>
        <Button  onClick={() => fetchRandomDog()} size="small">Changer de chien</Button>
      </CardActions> 
    </Card>
  )
}

export default RandomAnimals;
